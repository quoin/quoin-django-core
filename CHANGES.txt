v0.1.6, 2013.09.13 -- Merging all repos into one to avoid namespace trouble.
                        Added test report.
                        Refactored some fabric tasks.
v0.1.0, 2013.08.15 -- Initial release.
