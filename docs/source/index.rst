.. Quoin Django Core documentation master file, created by
   sphinx-quickstart on Mon Sep 23 11:10:34 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Quoin Django Core's documentation!
=============================================

This is the starting point documentation on how to setup a new django project.
There are some default tools and processes defined.

Contents:

.. toctree::
   :maxdepth: 2

   project
   django
   fabfile
   testing




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

