Unit Testing
------------

Generally tests should be in a directory underneath whichever django 'app' they are testing.
A good test suite will have both django client testing (view tests) as well as selenium tests.
An example test structure might look as follows::

    django/
        qi/
            app1/
                tests/
                    views/
                        home_test.py
                        contact_test.py
                    selenium/
                        test_home.py
                        test_admin.py


View Testing
============

Django includes a client that can be used to simulate real browser activity. Note that it is not
a full simulation, for real browser-driven testing, we use selenium.

class core.QuoinViewTestCase:

All View test cases should inherit from this class. It's recommended to then have a base class with anything
project specific, which inherits from this class, for example::

    QuoinMarketingTestCase(QuoinViewTestCase):
        ... Project-specific-methods 

Individual test cases should then inherit from the project-level base class.

FlatPage tests:
If your project contains any use of the django flatpages app, this test will run through and test that all
flatpage urls are visible and return content.


Selenium Testing
================

In order to fully utilize selenium testing, the base library `quoin-django-core` offers some base classes
that can be used to make testing easier.

core.QuoinSeleniumTestCase

This is the base testcase class that selenium testcases should inherit from.

`quoin-django-core` also contains a Page class that should be inherited from to create Pages representing a url
visited on the site.

For example::

    .. TODO : link to example of a pages.py file from Marketing ?

Once you have defined a number of pages for an app, you can use them in a test case::

    .. TODO : link to example of selenium test case

