How to setup Fabric?
====================

To use fabric, you need to create a `fabfile` directory at the base of the project.

This is the content that should be in `fabfile/__init__.py`::

    from quoin.core import fabfile as core

    # import predefined submodules
    core.update(__import__('sys').modules[__name__], 'app.settings.test')

    # import project's submodules
    #from fabfile import fixtures

A `fabfile/active.py` file is also required to overwrite the generic values::

    from fabric import api

    # Command to activate the virtualenv
    api.env.activate = '''source /your/path/to/project/django/virtualenv/bin/activate'''

    # Directory to change to after activating the virtualenv
    api.env.directory = '''/your/path/to/project/django'''

    # Name of environment, currently used by pip
    api.env.name = 'dev'


Once this is set, running `fab -l` will show you some generic tasks we want on
all of our projects.
