Setting Up A New Project
========================

This is the starting point documentation on how to setup a new project.


Repository
----------

We need to create a new repository in the `Quoin's account on bitbucket <https://bitbucket.org/quoin/>`_.

#.  Specify steps.
#.  Naming convention
#.  Private repo
#.  Language = Python
#.  Add admin privs to some members.
#.  Add write privs to some members.


On computer
-----------

Clone the repo to work::

    git clone git@bitbucket.org:quoin/project-name.git
    cd project-name


We want to keep the `master` as clean as possible for production. Create the
`develop` branch::

    git checkout -b develop origin/master
    git push origin develop
    git fetch origin


And now, create a "feature" branch to setup the project::

    git checkout -b <your_initials>/base-structure origin/develop

.. note::

    Note you may also create your own fork of the repository using Bitbucket's 'fork' feature

Settings up components
----------------------

Setting up the different components:

#. :doc:`django`


Commit the base structure
-------------------------

Now that everything is defined, commit the new structure and push it to
bitbucket::

    git push origin <your_initials>/base-structure


Getting the pull request ready and wait until someone approves and merge it.

#.  Step to create pull request.
#.  Add a title
#.  Add a comment/description.
#.  Close branch on merge.


