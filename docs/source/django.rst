Setting Up A New Django Project
===============================

This is the starting point documentation on how to setup django stuffs. We
should already be positioned at the top-level of the repo if we followed
instructions from :doc:`project`.


Default structure
-----------------

TODO: We should have a fabric script to create this to reduce human errors.
This is basically what it needs to do. All following documentation assume
virtualenv is setup as follow (assuming `virtualenv` and `pip` have been
installed site-wide)::

    # Create a virtualenv
    virtualenv virtualenv
    source virtualenv/bin/activate
    pip install -U setuptools

    # Install quoin-django-core into new virtualenv
    # This will install the base python packages required to run things like fabric, etc
    pip install git+ssh://git@bitbucket.org/quoin/quoin-django-core.git


    mkdir django
    django-admin.py startproject project_name django    # notice dot is important.
    cd django
    mkdir qi                                            # qi for Quoin Inc.
    touch qi/__init__.py                                # Make python package.
    mv project_name qi/                                 # Move under 'qi'.
    cd qi/project_name

Edit the `manage.py` to reflect the new package::

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "qi.project_name.settings")


Django settings
---------------

We use a package-like settings to allow multiple settings to be used::

    mkdir settings
    touch settings/__init__.py
    mv settings.py settings/default.py


Directory Structure
-------------------

At the end of this we should have a directory structure something like this::

    # base top level directory of repository
    /project_name
        /django
            manage.py
            /qi
                /project_name # this is your base django 'app'
                    /settings
                    /templates
                    /tests
                    models.py
                    views.py
                    urls.py
        # other directories related to project but not django specific
        /fabfile
        /doc
        /gunicorn
        /bin
        /virtualenv # your virtualenv directory (can be named whatever but must always be sourced)

Anything django specific will go into /django/qi/<app> while anything project specific will go into a top level directory

settings/default.py
^^^^^^^^^^^^^^^^^^^

This file needs to be changed to reflect the new level of package::

    ROOT_URLCONF = 'qi.project_name.urls'
    WSGI_APPLICATION = 'qi.project_name.wsgi.application'


settings/project.py
^^^^^^^^^^^^^^^^^^^

This file contains the project specific definition of the settings. This allows
us to more easily know what has been changed from the original settings file
generated by Django.

This file should contain::

    from qi.project_name.settings.default import *

    ...TODO: list of settings we want to overwrite...

    # Any setting that is not default should be specified here..

    # This line
    __import__('quoin.core.settings.project').update(__import__('sys').modules[__name__])


settings/production.py.txt
^^^^^^^^^^^^^^^^^^^^^^^^^^

Create the file that will contain settings for the production environment. The
file is named `*.py.txt` because we do not want it to be imported/compiled
automatically by tools like nose.

    ...TODO: Add a sample of what production should look like...


settings/qa.py.txt
^^^^^^^^^^^^^^^^^^

Create the file that will contain settings for the QA environment.

Sample of QA settings file::

    # QA Environment Settings

    from qi.project_name.settings.default import * # pylint: disable=W0614,W0401


    # Hosts/domain names that are valid for this site; required if DEBUG is False
    # See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
    ALLOWED_HOSTS = ['50.195.19.66', '10.1.10.31', '10.1.10.37',]


    # Services VM
    SERVICES_IP = '127.0.0.1'


    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'project_name',
            'USER': 'postgres',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '5432',
        }
    }


    # Absolute filesystem path to the directory that will hold user-uploaded files.
    MEDIA_ROOT = '/path/to/media/'


    # Absolute path to the directory static files should be collected to.
    STATIC_ROOT = '/path/to/static/'


    PIPELINE_SASS_BINARY = '/usr/local/bin/sass'

    PIPELINE_COFFEE_SCRIPT_BINARY = '/usr/local/bin/coffee'


    ################################################################
    ####                        Memcached                        ###
    ################################################################
    CACHES['default']['LOCATION'] = '%(ip)s:11211' % {'ip':SERVICES_IP}


    ################################################################
    ####                         Search                          ###
    ################################################################
    HAYSTACK_CONNECTIONS['default']['URL'] = 'http://%(ip)s:9200/' % {'ip':SERVICES_IP}


    ################################################################
    ####                         Logging                         ###
    ################################################################
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s',
            },
            'generic': {
                'format': '%(asctime)s [%(process)d] [%(levelname)s] %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S',
                'class': 'logging.Formatter',
            },
        },
        'handlers': {
            'console': {
                'level': 'WARN',
                'class': 'logging.StreamHandler',
            },
            'error': {
                'class': 'logging.FileHandler',
                'formatter': 'generic',
                'filename': '/home/quoin/logs/gunicorn.error.log',
            },
            'access': {
                'class': 'logging.FileHandler',
                'formatter': 'generic',
                'filename': '/home/quoin/logs/gunicorn.access.log',
            },
            'test': {
                'class': 'logging.FileHandler',
                'formatter': 'generic',
                'filename': '/home/quoin/logs/test.log',
            },

        },
        'loggers': {
            'django': {
                'handlers': ['console'],
                'level': 'WARN',
                'propagate': True,
            },
            'core': {
                'handlers': ['console', 'access', 'error',],
                'level': 'WARN',
                'propagate': True,
            },
            'gunicorn.error': {
                'level': 'INFO',
                'handlers': ['error'],
                'propagate': True,
            },
            'gunicorn.access': {
                'level': 'DEBUG',
                'handlers': ['access'],
                'propagate': False,
            },
        }
    }

settings/dev.py.txt
^^^^^^^^^^^^^^^^^^^

Create the file that will contain settings for the development environment.

Sample dev settings file::

    """
    Dev Environment Settings
    """

    import os
    from qi.marketing.settings.base import * # pylint: disable=W0401,W0614

    # Hosts/domain names that are valid for this site; required if DEBUG is False
    # See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
    ALLOWED_HOSTS = ['*']


    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'marketing',
            'USER': 'postgres',
            'PASSWORD': '',
            'HOST': '', # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
            'PORT': '',
        }
    }

    DEBUG = True
    TEMPLATE_DEBUG = DEBUG

    PROJECT_DJANGO_ROOT = os.path.abspath(os.path.join(PROJECT_ROOT, '..', '..'))
    RUNNING_DJANGO_DIR = '/home/dev'


    # Absolute filesystem path to the directory that will hold user-uploaded files.
    # Example: "/var/www/example.com/media/"
    MEDIA_ROOT = os.path.join(RUNNING_DJANGO_DIR, 'media')


    # Absolute path to the directory static files should be collected to.
    # Don't put anything in this directory yourself; store your static files
    # in apps' "static/" subdirectories and in STATICFILES_DIRS.
    # Example: "/var/www/example.com/static/"
    STATIC_ROOT = os.path.join(RUNNING_DJANGO_DIR, 'static')


    PIPELINE_SASS_BINARY = '/usr/bin/sass'

    PIPELINE_COFFEE_SCRIPT_BINARY = '/usr/bin/coffee'


    ################################################################
    ####                      Debug Toolbar                      ###
    ################################################################
    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )

    INSTALLED_APPS += (
        'django_nose',
        'debug_toolbar',
        'quoin.core.reports',
    )

    DEBUG_TOOLBAR_PANELS = (
        'debug_toolbar.panels.version.VersionDebugPanel',
        'debug_toolbar.panels.timer.TimerDebugPanel',
        'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
        'debug_toolbar.panels.headers.HeaderDebugPanel',
        'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
        'debug_toolbar.panels.template.TemplateDebugPanel',
        'debug_toolbar.panels.sql.SQLDebugPanel',
        'debug_toolbar.panels.signals.SignalDebugPanel',
        'debug_toolbar.panels.logger.LoggingPanel',
        'debug_toolbar.panels.profiling.ProfilingDebugPanel',
    )

    def show_debug_toolbar(request): # pylint: disable=W0613
        """
        .
        """
        return False

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
        'SHOW_TOOLBAR_CALLBACK': show_debug_toolbar,
        'HIDE_DJANGO_SQL': False,
        'TAG': 'div',
        'ENABLE_STACKTRACES' : True,
    }


    ################################################################
    ####                        Selenium                         ###
    ################################################################
    BASE_URL = None # Define in local.py
    LOG_ROOT = os.path.join(RUNNING_DJANGO_DIR, 'logs')

    # override LOGGING settings if needed

settings/test.py
^^^^^^^^^^^^^^^^

Here is the content of the `settings/test.py` file::

    #!/usr/bin/env python
    # -*- mode: Python; coding: UTF-8 -*-

    """\
    Test settings. Run as:

        fab test

    This should only be used to quick dev test. This should not be used before
    deploy because of known issues with SQLite3 backends (lenght is not respected,
    among others).
    """



    __docformat__ = 'restructuredtext en'

    __all__ = (
            )



    # Import current settings.
    from qi.marketing.settings import * # pylint: disable=W0614,W0401



    CACHE_MIDDLEWARE_KEY_PREFIX = 'qi-project_name-test'

    # other settings as needed for test environment

    # Adding quoin.core.settings
    from quoin.core.settings import test
    test.update(__import__('sys').modules[__name__])


settings/local.py
^^^^^^^^^^^^^^^^^

These settings are local to your individual installation, they should never be committed back to the repository.
Example settings from a settings/local.py file::

    DEBUG = True
    RUNNING_DJANGO_DIR = '/home/dev/'
    MEDIA_ROOT = '/home/dev/media/'
    STATIC_ROOT = '/home/dev/static/'
    TEMPLATE_DEBUG = DEBUG

    HOST_IP = '10.1.10.15'
    BASE_URL = 'http://{0}'.format(HOST_IP)
    SELENIUM_HUB_IP = '10.1.10.28'
    SELENIUM_HUB = 'http://{0}:4444/wd/hub'.format(SELENIUM_HUB_IP)
    SELENIUM_WAIT_TIMEOUT = 5
    SERVICES_IP = '127.0.0.1'

