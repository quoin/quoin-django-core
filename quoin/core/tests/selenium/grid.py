#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Module to extract and parse selenium grid info.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.18
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        )



import bs4
import requests

from django.conf import settings



def node_info_from_img(img_tag):
    """
    Retrieves the node information from the image tag.
    """
    title = img_tag.attrs['title'].replace('{', '').replace('}', '')
    try:
        d = dict([data.split('=') for data in title.split(', ')])
        return (d['browserName'], d['platform'], d.get('version', ''))
    except:
        # A node is busy? Just ignore it.
        return None



def parse_grid():
    """
    Parse the grid console page and extract all info.
    """
    try:
        console_url = settings.SELENIUM_HUB.replace('/wd/hub', '/grid/console')
    except AttributeError:
        raise EnvironmentError("Need to define SELENIUM_HUB.")

    response = requests.get(console_url)
    if (200 != response.status_code):
        raise EnvironmentError("Cannot access page: {0!s}".format(response.reason))

    soup = bs4.BeautifulSoup(response.text)
    nodes = set(node_info_from_img(img) for img in soup.find_all('img'))
    try:
        nodes.remove(None)
    except KeyError:
        # No errors parsing hub.
        pass

    return sorted(nodes)
