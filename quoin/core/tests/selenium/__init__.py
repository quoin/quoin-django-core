#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

# pylint: disable=R0904

"""
Selenium base TestCase.
"""



from quoin.core.tests.selenium.page import * # pylint: disable=W0401
from quoin.core.tests.selenium.quoin_selenium_test_case import * # pylint: disable=W0401
