#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Base page module for selenium tests.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.09
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'Page',
        )



import datetime
import inspect
import logging
import os
import urlparse

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from django.conf import settings



class Page(object):
    """
    A single HTML page.
    """

    title = ''  # set by subclass
    url = ''    # set by subclass

    # Test case that requested the page.
    test_case = None

    logger = logging.getLogger('test')

    TITLE_ERROR = """Expected title "{0!s}" did not match actual title "{1!s}" for URL {2!s}"."""
    GET_ELEMENT_ERROR = """Select by "{0!s}" using "{1!s}" with require_visible={2!r} not found on URL "{3!s}"."""


    def __init__(self, selenium, test_case, need_to_load_page, *args, **kwargs):
        """
        Store the driver, potentially open self.
        """

        self.selenium = selenium
        self.test_case = test_case
        if need_to_load_page:
            self.selenium.get(urlparse.urljoin(settings.BASE_URL, self.url))

        err_message = self.TITLE_ERROR.format(self.title, self.selenium.title, self.url)

        try:
            expected_condition = EC.title_contains(self.title)
            WebDriverWait(self.selenium, settings.SELENIUM_WAIT_TIMEOUT).until(expected_condition, message=err_message)
        except TimeoutException:
            self.logger.error(err_message)
            filename = self.save_screenshot()
            self.logger.error('    Screenshot file in log directory: {0!r}.'.format(filename))
            self.test_case.fail(err_message)

    def get_page(self, class_, *args, **kwargs):
        """
        Gets another page from the current page. This sets `need_to_load_page`
        to `False` by default because the browser should already be open at
        this state.
        """
        return class_(self.selenium, self.test_case, False, *args, **kwargs)


    def click_link(self, text, partial=False):
        """
        Click a link.
            text - the text of the link to click
            partial - True if the text arg is a substring of the entire link text
        """
        select_by = By.LINK_TEXT if not partial else By.PARTIAL_LINK_TEXT
        self.get_element(select_by, text).click()

        # TODO - factory for Pages so we can reverse lookup based on url and automagically return an instance of the right Page
        #return factory.get_page(text)

    def get_element(self, select_by, selector, require_visible=True):
        """
        Get an element.
            select_by - how you are selecting the element. one of:
                By.CLASS_NAME, By.CSS_SELECTOR, By.ID, By.LINK_TEXT,
                By.NAME, By.PARTIAL_LINK_TEXT, By.TAG_NAME, By.XPATH
            selector - the selector itself
            require_visible - True if the element must also be visible

            e.g. get_element(By.ID, 'foo') is equivalent to $('#foo')
        """
        err_message = self.GET_ELEMENT_ERROR.format(select_by, selector, require_visible, self.url)
        try:
            if require_visible:
                expected_condition = EC.visibility_of_element_located((select_by, selector))
            else:
                expected_condition = EC.presence_of_element_located((select_by, selector))
            return WebDriverWait(self.selenium, settings.SELENIUM_WAIT_TIMEOUT).until(expected_condition, message=err_message)
        except TimeoutException as err:
            self.logger.error(err_message)
            filename = self.save_screenshot()
            self.logger.error('    Screenshot file in log directory: {0!r}.'.format(filename))
            self.test_case.fail(err_message)


    def get_element_by_class_name(self, class_name, require_visible=True):
        """
        Gets an element by class name.
        """
        return self.get_element(By.CLASS_NAME, class_name, require_visible)


    def get_element_by_css_selector(self, css_selector, require_visible=True):
        """
        Gets an element by CSS selector.
        """
        return self.get_element(By.CSS_SELECTOR, css_selector, require_visible)


    def get_element_by_id(self, id, require_visible=True):
        """
        Gets an element by ID.
        """
        return self.get_element(By.ID, id, require_visible)


    def get_element_by_link_text(self, link_text, require_visible=True):
        """
        Gets an element by text on a link.
        """
        return self.get_element(By.LINK_TEXT, link_text, require_visible)


    def get_element_by_name(self, name, require_visible=True):
        """
        Gets an element by name of a field.
        """
        return self.get_element(By.NAME, name, require_visible=True)


    def get_element_by_partial_link_text(self, partial_link_text, require_visible=True):
        """
        Gets an element by partial text on a link.
        """
        return self.get_element(By.PARTIAL_LINK_TEXT, partial_link_text, require_visible)


    def get_element_by_tag_name(self, tag_name, require_visible=True):
        """
        Gets an element by tag name.
        """
        return self.get_element(By.TAG_NAME, tag_name, require_visible)


    def get_element_by_xpath(self, xpath, require_visible=True):
        """
        Gets an element by XPATH.
        """
        return self.get_element(By.XPATH, xpath, require_visible)


    def save_screenshot(self):
        """
        Save a timestamped screenshot and return the filename.
        """
        _calling_method = inspect.stack()[1][3]
        filename = '%s.%s.png' % (datetime.datetime.now().strftime('%s'), _calling_method)
        self.selenium.get_screenshot_as_file(os.path.join(os.environ[settings.CURRENT_TEST_DIR_ENV_NAME], filename))
        self.test_case.log_screenshot(filename)
        return filename
