#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Base test case for seleniums tests.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.09
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'QuoinSeleniumTestCase',
        )



import os

from selenium import webdriver

from django.conf import settings
from django.test import LiveServerTestCase

from quoin.core.tests import mixin



class QuoinSeleniumTestCase(LiveServerTestCase, mixin.TestCaseLogMixin):
    """
    Manages selenium tests connection to the remote web driver.
    """

    @staticmethod
    def get_capabilities():
        """
        Read an environment variable to determine the browser under test.
        """
        capabilities = {
            'browserName': os.environ.get('SELENIUM_BROWSER', 'firefox').lower(),
            'platform': os.environ.get('SELENIUM_PLATFORM', 'ANY'),
            'version': os.environ.get('SELENIUM_BROWSER_VERSION', ''),
            'javascriptEnabled': True,
        }
        return capabilities

    def setUp(self):
        """
        Create the remote selenium driver.
        """
        settings.DEBUG = True
        self.log_start()
        self.selenium = webdriver.Remote(
            command_executor=settings.SELENIUM_HUB,
            desired_capabilities=self.get_capabilities())
        super(QuoinSeleniumTestCase, self).setUp()

    def tearDown(self):
        """
        Destroy the remote selenium driver.
        """
        self.log_end()
        self.selenium.close()
        self.selenium.quit()
        super(QuoinSeleniumTestCase, self).tearDown()

    def get_page(self, class_, *args, **kwargs):
        """
        Gets the page in the test case. Pages open within a test case will
        call to open the URL of the `class_`.
        """
        return class_(self.selenium, self, True, *args, **kwargs)



