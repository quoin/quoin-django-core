#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Basic mixin for tests.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.05
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        )



import logging



class TestCaseLogMixin(object):
    """
    Some test case mixin to add logging.
    """

    _test_logger = logging.getLogger('testrun')

    def info(self, msg):
        """
        Adding an INFO entry to the log file.
        """
        self._test_logger.info(msg, extra={'testid': self.id()}) # id() comes from TestCase.


    def log_start(self):
        """
        Add a start log.
        """
        self.info("Start")


    def log_end(self):
        """
        Add an end log.
        """
        self.info("End")


    def log_screenshot(self, filename):
        """
        Add a log of a saved screenshot.
        """
        self.info("    <screenshot>{0!s}</screenshot>".format(filename))
