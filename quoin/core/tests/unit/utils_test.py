#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

# pylint: disable=E1103,R0901,R0904

"""\
Unit tests for quoin.core.utils module

Author: Evan LeCompte <evan.lecompte@quoininc.com>
Since: 2013.09.19
Organization: Quoin Inc.
Copyrights:
"""

__docformat__ = 'restructuredtext en'

__all__ = (
        'UtilsTestCase',
        'TestGetDocString',
        )

import inspect

from quoin.core.tests import unit
from quoin.core import utils

class UtilsTestCase(unit.QuoinUnitTestCase):
    """
    Tests functions in quoin.core.utils
    """

class TestGetDocString(UtilsTestCase):
    """
    Tests get_docstring() function.
    """
    error_msg = 'Error getting docstring.'

    def test_01_long_path_returns_correct_docstring(self):
        """
        Test long module path, 1 class, correct method.
        """
        result = utils.get_docstring('django.contrib.sites.models.SiteManager', 'get_current')

        from django.contrib.sites.models import SiteManager
        expected = inspect.getdoc(SiteManager.get_current)

        self.assertEqual(result, expected)

    def test_02_short_path_returns_correct_docstring(self):
        """
        Test short module path, 1 class, correct method.
        """
        result = utils.get_docstring('logging.Logger', 'setLevel')

        from logging import Logger
        expected = inspect.getdoc(Logger.setLevel)

        self.assertEqual(result, expected)

    def test_03_single_class_returns_errormsg(self):
        """
        Test single class, no module.
        """
        result = utils.get_docstring('Logger', 'setLevel')
        self.assertEqual(result, self.error_msg)

    def test_04_class_within_class_returns_errormsg(self):
        """
        Test class within a class fails.
        """
        result = utils.get_docstring('django.contrib.sites.models.Site.Meta', 'add_field')
        self.assertEqual(result, self.error_msg)

    def test_05_nonexistent_method_returns_errormsg(self):
        """
        Test non-existent method.
        """
        result = utils.get_docstring('logging.Logger', 'fooBar')
        self.assertEqual(result, self.error_msg)

    def test_06_non_string_method_arg_returns_errormsg(self):
        """
        Test non-string method arg.
        """
        result = utils.get_docstring('logging.Logger', 4)
        self.assertEqual(result, self.error_msg)

    def test_07_non_string_classpath_arg_returns_errormsg(self):
        """
        Test non-string classpath arg.
        """
        result = utils.get_docstring(4, 'setLevel')
        self.assertEqual(result, self.error_msg)

    def test_08_None_method_arg_returns_errormsg(self):
        """
        Test None as method arg.
        """
        result = utils.get_docstring('setLevel', None)
        self.assertEqual(result, self.error_msg)

    def test_09_None_classpath_arg_returns_errormsg(self):
        """
        Test None as classpath arg.
        """
        result = utils.get_docstring(None, 'setLevel')
        self.assertEqual(result, self.error_msg)

