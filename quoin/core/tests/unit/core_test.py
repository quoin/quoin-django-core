#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Tests for the core modules.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.16
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'AllViewsTest',
        )



from django.conf import settings

from quoin.core.tests import unit
from quoin.core import utils



class AllViewsTest(unit.QuoinUnitTestCase): # pylint: disable=R0901,R0904
    """
    Tests that all URLs are hit.
    """


    def test_00_project_urls_have_names(self):
        """
        Makes sure all app views from settings.PROJECT_APPS have url_name defined.
        """
        errors = list()
        for (regex, _, module_, url_name) in utils.reduce_urlpatterns():
            if (module_.startswith(settings.PROJECT_APPS) and (url_name is None)):
                errors.append(regex)
        self.failIf(errors, msg="Missing name for following URLs:\n  - {0}\nTotal: {1}".format("\n  - ".join(errors), len(errors),))


    def test_01_project_urls_are_tested(self):
        """
        Makes sure all URL are tested.

        This will look through all apps from settings.PROJECT_APPS and go
        into the tests/views directory and assume each file.py to be a view
        test (url_name_test.py).
        """
        errors = list()
        for (regex, _, module_, url_name) in utils.reduce_urlpatterns():
            if (url_name):
                test_module_prefix = url_name.replace('-', '_')
                for app in settings.PROJECT_APPS:
                    try:
                        __import__('.'.join([app, 'tests.views', '{0!s}_test'.format(test_module_prefix)]), {}, {}, [''])
                        break
                    except ImportError:
                        pass
                else:
                    for app in settings.PROJECT_APPS:
                        if (module_.startswith(app)):
                            errors.append("url name {0!r} is not tested. Add {1!s}.".format(url_name, '/'.join(app.split('.') + ['tests', 'views', '{0}_test.py'.format(test_module_prefix)])))
                            break
                    else:
                        errors.append("url name {0!r} is not tested.".format(url_name))
            else:
                errors.append("url regex {0!r} is not tested in TESTED_URL_NAMES.".format(regex))
        self.failIf(errors, msg="Following views tests are missing:\n  - {0}\nTotal: {1}".format("\n  - ".join(errors), len(errors)))
