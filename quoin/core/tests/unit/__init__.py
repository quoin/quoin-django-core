#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Base TestCase for Quoin unit tests.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.28
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'QuoinUnitTestCase',
        )



import re

from django import test
from django.conf import settings

from quoin.core.tests import mixin



class QuoinUnitTestCase(test.TestCase, mixin.TestCaseLogMixin): # pylint: disable=R0904
    """
    Quoin base test case.
    """


    def setUp(self):
        settings.DEBUG = True
        self.log_start()
        super(QuoinUnitTestCase, self).setUp()


    def tearDown(self):
        self.log_end()
        super(QuoinUnitTestCase, self).tearDown()


    def assertQuerySetIsEmpty(self, obtained): # pylint: disable=C0103
        """
        Makes sure the resulting queryset is empty.
        """
        super(QuoinUnitTestCase, self).assertEqual(obtained.count(), 0)


    def assertQuerySetCount(self, obtained_qs, expected_count): # pylint: disable=C0103
        """
        Validates that the queryset count is equal to the `expected_count`.
        """
        super(QuoinUnitTestCase, self).assertEqual(obtained_qs.count(), expected_count)


    def assertUnicodeStrReprFormat(self, instance): # pylint: disable=C0103
        """
        Validates that the instance's unicode(), str() and repr() is not
        throwing any errors.
        """
        self.assertIsInstance(unicode(instance), unicode)
        self.assertIsInstance(str(instance), str)
        self.assertIsInstance(repr(instance), str)
        self.assertIsNotNone(re.match(r'^<%s\(.*\) at 0x[0-9a-f]+>$'.format(instance.__class__.__name__,), repr(instance)))



