#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

# pylint: disable=W0105,W0221

"""\
Base TestCase for Quoin views.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.28
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'QuoinViewTestCase',
        )



import bs4

from django import test
from django.core import cache
from django.core import urlresolvers

from quoin.core.tests import unit



class QuoinViewTestCase(unit.QuoinUnitTestCase): # pylint: disable=R0904
    """
    Quoin base test case for view.

    This subclass predefine `self.client` to be ready to connect. It will
    also predefine `self.url` if `URL_NAME` is not `None`.
    """

    """Url name of the view to test."""
    URL_NAME = None

    """If URL needs parameters."""
    URL_NEEDS_KWARGS = False

    """Test client to connect to the view."""
    client = None

    """URL of the view to test. This is defined if `URL_NAME` is not `None`."""
    url = None

    """Username to use when login is required."""
    default_username = None

    """Password to use when login is required."""
    default_password = None


    def get(self, with_login=False, username=None, password=None):
        """
        Gets the `self.url`.
        """
        if (with_login):
            login = self.client.login(username=(username or self.default_username), password=(password or self.default_password))
            self.assertTrue(login)
        return self.client.get(self.url)


    def setUp(self, url=None):
        super(QuoinViewTestCase, self).setUp()
        self.client = test.Client()
        self.url = url

        if (self.url is None):
            if ((self.URL_NAME is not None) and not (self.URL_NEEDS_KWARGS)):
                self.url = urlresolvers.reverse(self.URL_NAME)

        if (self.url is None):
            self.fail("self.url is not defined.")


    def tearDown(self):
        super(QuoinViewTestCase, self).tearDown()

        # Make sure the cache is reset between tests.
        cache.get_cache('default').clear()


    @staticmethod
    def get_soup(resp):
        """
        Gets the soup for the response.
        """
        return bs4.BeautifulSoup(resp.content)
