#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

# pylint: disable=E1103,R0901,R0904

"""\
Tests any django flatpages created in an application.

Author: Evan LeCompte <evan.lecompte@quoininc.com>
Since: 2013.09.18
Organization: Quoin Inc.
Copyrights:
"""

__docformat__ = 'restructuredtext en'

__all__ = (
        'FlatPagesTestCase',
        )


from django import test
from django.core import cache
from django.contrib.flatpages import models

from quoin.core.tests import unit

class FlatPagesTestCase(unit.QuoinUnitTestCase):
    """
    Tests all flatpages are accessible and render correctly.
    Meant to be subclassed in a django project, with flatpage
    objects loaded by fixtures.
    """

    """ The flatpages to test """
    flatpages_ = None

    def setUp(self):
        super(FlatPagesTestCase, self).setUp()
        self.flatpages_ = models.FlatPage.objects.all()

        if (self.flatpages_ is None):
            self.fail('self.flatpages_ is not defined.')

    def tearDown(self):
        super(FlatPagesTestCase, self).tearDown()

        # Make sure the cache is reset between tests.
        cache.get_cache('default').clear()

    def test_access(self):
        """
        Test urls work
        """
        for flatpage_ in self.flatpages_:
            self.url = flatpage_.get_absolute_url()
            resp = self.get()
            self.assertEqual(resp.status_code, 200)
            self.assertTemplateUsed(flatpage_.template_name)

    def test_cache(self):
        """
        Verifies that the cache is hit.
        """
        # Hit pages once.
        self.test_access()

        for flatpage_ in self.flatpages_:
            self.url = flatpage_.get_absolute_url()
            resp = self.get()
            self.assertEqual(resp.status_code, 200)
            self.assertTemplateNotUsed(flatpage_.template_name)

    def test_render(self):
        """
        Test template rendering of flatpage object
        """
        for flatpage_ in self.flatpages_:
            self.url = flatpage_.get_absolute_url()
            resp = self.get()
            _main_div = self.get_main_content_soup(resp)
            dom_fp = _main_div.select('div.row-fluid.margined')
            self.assertEqual(dom_fp[0].renderContents()[:20], flatpage_.content[:20])
