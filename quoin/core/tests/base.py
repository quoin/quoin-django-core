#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Base TestCase for Quoin unit tests. Unit tests with Django should use
`quoin.core.tests.unit`.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.30
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'QuoinUnittestTestCase',
        'suite',
        )



import unittest



class QuoinUnittestTestCase(unittest.TestCase): # pylint: disable=R0904
    """
    Quoin's base unittest TestCase.

    This class should only be extended in projects where Django is not used.
    """


    def assertUnicodeStrReprFormat(self, instance_): # pylint: disable=C0103
        """
        Validates that the instance's unicode(), str() and repr() format do not
        have errors.
        """
        self.assertIsInstance(unicode(instance_), unicode)
        self.assertIsInstance(str(instance_), str)
        self.assertIsInstance(repr(instance_), str)



def suite(modules):
    """
    Defines a test suite for each module in `modules`.

    This is useful if we are not using any test discovery tools. In the package
    `__init__.py` file, you would typically have:

        >>> from quoin.core.tests.base
        >>> from this_package import subpackage
        >>> from this_package import a_module
        >>> def suite():
        ...     return base.suite([
        ...             subpackage,
        ...             a_module,
        ...             ])
    """
    tests = unittest.TestSuite()
    test_loader = unittest.defaultTestLoader

    for module_ in modules:
        tests.addTests(test_loader.loadTestsFromModule(module_))
        try:
            tests.addTests(module_.suite())
        except AttributeError:
            pass

    return tests
