#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Default settings for tests.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.06
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'update',
        )



import os



def update(module):
    """
    Modify the settings with test specific settings.
    """

    try:
        module.LOG_ROOT
    except AttributeError:
        raise EnvironmentError("Missing LOG_ROOT setting.")

    module.DEBUG = True
    module.TEMPLATE_DEBUG = module.DEBUG
    module.PIPLINE = False

    module.DATABASES['default'] = {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': ':memory:',
            }



    #   Handlers

    module.LOGGING['handlers']['null'] = {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
            }


    #   Loggers

    module.LOGGING['loggers']['django.db.backends'] = {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
            }

    module.LOGGING['loggers']['south'] = {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
            }



    class InvalidString(str): # pylint: disable=R0904
        """
        Raises an error instead of silently forgiving.
        """
        def __mod__(self, thing):
            """
            Error when this is called.
            """
            from django.template import base
            raise base.TemplateSyntaxError("Undefined variable or unknown value for: {0!r}.".format(thing))

    module.TEMPLATE_STRING_IF_INVALID = InvalidString('%s')


    from quoin.core.reports.settings import test as test_settings
    test_settings.update(module)

