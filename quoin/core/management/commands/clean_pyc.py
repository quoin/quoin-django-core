#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Management command to delete all *.py[co] files.
"""



import glob
import optparse
import os

from django.core.management.base import BaseCommand



class Command(BaseCommand):
    """\
    """

    option_list = BaseCommand.option_list + (
            optparse.make_option('--dry-run', '-n', action='store_true', dest='dry_run', help="""Do not delete. Display what would be deleted."""),

            )

    help = "Clean all *.pyc and *.pyo files."

    requires_model_validation = False


    @classmethod
    def find_top_dir(cls):
        """\
        Finds the top directory according to the current module path.
        """
        len_module = len(cls.__module__.split('.'))
        return os.path.join(os.path.sep, *__file__.split(os.path.sep)[:-len_module])


    def items(self):
        for (dirpath, dirnames, filenames) in os.walk(self.find_top_dir()):
            for filename in glob.glob(os.path.join(dirpath, '*.py[co]')):
                if (os.path.isfile(filename)):
                    yield filename


    def handle(self, *args, **options):
        """\
        """
        if ('3' == options.get('verbosity')):
            self.stdout.write("handle(*args={0!r}, **options={1!r})".format(args, options))
        top_dir = self.find_top_dir()
        for filename in self.items():
            f = filename.replace(top_dir, '')
            if (options.get('dry_run')):
                if ('3' == options.get('verbosity')):
                    self.stdout.write("{0!r} would be removed.".format(f))
            else:
                os.remove(filename)
                if ('3' == options.get('verbosity')):
                    self.stdout.write("{0!r} removed.".format(f))
