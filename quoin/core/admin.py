#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""
Core admin configuration.

Author: Quoin
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__metaclass__ = type


__all__ = (
        )



from django.contrib import admin
from django.contrib.sessions import models as sessions_models
from reversion.admin import VersionAdmin
from south.models import MigrationHistory



class MigrationHistoryAdmin(admin.ModelAdmin):
    """
    Add South migration history to admin.
    """

    list_display = ('app_name', 'migration', 'applied')
admin.site.register(MigrationHistory, MigrationHistoryAdmin)



class SessionAdmin(admin.ModelAdmin):
    """
    Show session info in the admin.
    """

    def _session_data(self, obj):
        """
        Provide a function to be used in the admin display.
        """
        return obj.get_decoded()

    list_display = ['session_key', '_session_data', 'expire_date', ]
admin.site.register(sessions_models.Session, SessionAdmin)



# very slightly modified from http://django-tinymce.readthedocs.org/en/latest/usage.html#the-flatpages-link-list-view
from flatblocks.admin import FlatBlockAdmin
from flatblocks.models import FlatBlock
from tinymce.widgets import TinyMCE

class TinyMCEFlatBlockAdmin(FlatBlockAdmin, VersionAdmin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'content':
            return db_field.formfield(widget=TinyMCE(attrs={'cols': 100, 'rows': 20}))
        return super(TinyMCEFlatBlockAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.unregister(FlatBlock)
admin.site.register(FlatBlock, TinyMCEFlatBlockAdmin)



from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin

class TinyMCEFlatPageAdmin(FlatPageAdmin, VersionAdmin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'content':
            return db_field.formfield(widget=TinyMCE(attrs={'cols': 100, 'rows': 20}))
        return super(TinyMCEFlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, TinyMCEFlatPageAdmin)
