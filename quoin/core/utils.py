#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Utility functions.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.16
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'reduce_urlpatterns',
        'get_docstring',
        )



import re
import inspect

from django.conf import settings
from django.contrib.admindocs.views import simplify_regex

from django_extensions.management.commands import show_urls



def reduce_urlpatterns():
    """
    Gets urlpatterns in a reduced regex pattern, for all modules specified in
    settings.PROJECT_APPS.

    Returns a sorted list of 4-tuples: (`regex`, `func_name`, `module`, `url_name`).
    """
    # Logic from show_urls.Command.handle()
    if settings.ADMIN_FOR:
        settings_modules = [__import__(m, {}, {}, ['']) for m in settings.ADMIN_FOR]
    else:
        settings_modules = [settings]

    views = list()
    for settings_module in settings_modules:
        try:
            urlconf = __import__(settings_module.ROOT_URLCONF, {}, {}, [''])
        except ImportError as err:
            print("Error loading {0!s}: {1!s}".format(settings_module.ROOT_URLCONF, err))
            continue

        for (func, regex, url_name) in show_urls.extract_views_from_urlpatterns(urlconf.urlpatterns):
            if (not func.__module__.startswith(settings.PROJECT_APPS)):
                continue
            if (hasattr(func, '__name__')):
                # is a func.
                func_name = func.__name__
            elif (hasattr(func, '__class__')):
                # class view
                func_name = '{0!s}()'.format(func.__class__.__name__,)
            else:
                func_name = re.sub(r' at 0x[0-9a-f]+', '', repr(func))
            views.append((simplify_regex(regex), func_name, func.__module__, url_name or None))
    return sorted(views)


def get_docstring(classpath, methodname):
    """
    Gets the docstring for a given method, requires method name
    and full (absolute) classpath as strings.
    """
    try:
        (modulename, classname) = classpath.rsplit('.', 1)
        mod = __import__(modulename, fromlist=[classname])
        cls = getattr(mod, classname)
        return inspect.getdoc(getattr(cls, methodname))
    except:
        return "Error getting docstring."
