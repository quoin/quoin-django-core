#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Module to generate report.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.12
Organization: Quoin Inc.
Copyrights:
"""


__docformat__ = 'restructuredtext en'

__all__ = (
        'Report',
        )


from datetime import datetime
import os
import xunitparser

from django import template
from django.template import loader
from django.conf import settings

from quoin.core import reports
from quoin.core.reports.constants import DISPLAY_DATE_FORMAT, FILE_DATE_FORMAT


class Report(object):
    """
    Generate report from the test run.
    """

    __dirpath = None
    __test_cases = None
    __current_test_number = 1

    def __init__(self, dirpath):
        """
        Initialise a report.
        """
        self.__dirpath = dirpath
        self.__test_cases = dict()

    def __add(self, test_case, browser, platform, version):
        """
        Adds the test case.
        """
        name = test_case.id()
        if (name not in self.__test_cases):
            self.__test_cases[name] = reports.MultiTestCase()
        self.__test_cases[name].add(test_case, self.__current_test_number, browser, platform, version)
        self.__current_test_number += 1

    def load_file(self, filepath, browser=None, platform=None, version=None):
        """
        Loads the xunit report file, specifying which browser, platform
        and version it was tested on if applicable (only meaningful with
        Selenium tests).
        """
        if (not os.path.isfile(filepath)):
            print("Cannot find file {0}".format(filepath))
            return

        for test_case in xunitparser.parse(filepath)[0]:
            self.__add(test_case, browser, platform, version)

    @classmethod
    def generate(cls):
        """
        Generate report from the test run.
        """
        if (not reports.Directory.get_directory(settings)):
            raise reports.NoTestDirectoriesAvailable("No test directories under directory {0}".format(settings.TEST_REPORT_ROOTDIR))

        # The base directory.
        try:
            base_dir = os.path.dirname(settings.XUNIT_FILE)
            base_name = os.path.basename(settings.XUNIT_FILE)
        except AttributeError:
            raise EnvironmentError("Run this with test settings or define XUNIT_FILE.")

        # Fast way to obtain directories and filesnames separately.
        dirnames = os.walk(base_dir).next()[1]

        report = cls(base_dir)

        # Unit and views tests.
        report.load_file(settings.XUNIT_FILE)

        # Selenium tests in test.cross are under browser-platform-version subdirectory.
        for dirname in dirnames:
            info = dirname.split('-')
            if (3 == len(info)):
                (browser, platform, version) = info
                report.load_file(os.path.join(base_dir, dirname, base_name), browser, platform, version)

        return report.generate_files(base_dir)

    def generate_files(self, dirpath):
        """
        Generates the HTML report.
        """
        context = template.Context()
        context['test_suite'] = sorted(self.__test_cases.values())
        context['static_url'] = '{0}{1}'.format(settings.STATIC_URL, '' if (settings.STATIC_URL.endswith('/')) else '/')
        context['run_datetime'] = datetime.strptime(dirpath[-12:], FILE_DATE_FORMAT).strftime(DISPLAY_DATE_FORMAT)

        tmpl = loader.get_template('quoin/core/reports/index.html')
        content = tmpl.render(context)

        dest_file = os.path.join(dirpath, 'index.html')
        open(dest_file, 'wb').write(content)
        print("Report generated: {0}".format(dest_file))
        return None
