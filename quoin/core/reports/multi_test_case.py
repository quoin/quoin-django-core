#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
A test case containing (potential) multiple runs. This is to contain tests
across browsers, but also can handle unit and views tests.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.19
Organization: Quoin Inc.
Copyrights:
"""


__docformat__ = 'restructuredtext en'

__all__ = (
        'MultiTestCase',
        )


from quoin.core import utils
from quoin.core.reports import test_result
from quoin.core.reports.constants import NO_DOCSTRING


class MultiTestCase(object):
    """
    Class to represent a test case that has potentially been run on multiple
    browsers, platforms and versions.
    """

    #: Dictionary to contain all the runs.
    __runs = None

    #: Current test_case name.
    __name = None

    #: Current test_case description.
    __description = None

    def __init__(self):
        """
        Initialize some containers.
        """
        self.__runs = dict()

    def __repr__(self):
        """Representation of the test case."""
        return '<{0}(name={2!r}) at 0x{1:x}>'.format(
                '.'.join([self.__module__, self.__class__.__name__]),
                id(self),
                self.name,
                )

    def __cmp__(self, other):
        """Compare the two objects."""
        return cmp(self.name, other.name)

    @property
    def name(self):
        """Test case's name."""
        return self.__name

    @property
    def has_description(self):
        """Determines if the description exists."""
        return True if self.__description else False

    @property
    def description(self):
        """Test case's description."""
        return self.__description or NO_DOCSTRING

    @property
    def runs(self):
        """List of runs. Ordered."""
        return sorted(self.__runs.values())

    def add(self, test_case, number, browser, platform, version):
        """
        Adds a run on a specific browser, platform, version.
        """
        if (self.name is None):
            self.__name = test_case.id()

        if (self.__description is None):
            self.__description = utils.get_docstring(test_case.classname, test_case.methodname)

        key = (browser, platform, version)
        try:
            self.__runs[key]  # pylint: disable=W0104
            return  # If already run, ignore.
        except KeyError:
            self.__runs[key] = test_result.TestResult(test_case, number, browser, platform, version)
