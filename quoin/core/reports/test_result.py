#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Module for representing a test result.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.11
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'TestResult',
        )


import urlparse

from quoin.core.reports.constants import SCREENSHOT_REGEX


class TestResult(object):
    """
    Representation of a test run result. This is used when generating
    test reports.
    """

    # Single test.
    __test_case = None

    # List of screenshots associated with this test case.
    __screenshots = None

    __number = 0
    __browser = None
    __platform = None
    __version = None

    def __init__(self, test_case, number, browser, platform, version):
        """
        Initializes with `test_case`. `test_case` is an element of the test suite
        from the XUnit report.
        """
        self.__test_case = test_case
        self.__number = number
        self.__browser = browser
        self.__platform = platform
        self.__version = version
        self.__set_screenshots()

    def __cmp__(self, other):
        """Compare the two objects."""
        return cmp((self.browser, self.platform, self.version), (other.browser, other.platform, other.version))

    def __set_screenshots(self):
        """
        Appends the screenshot image. This will create a path to the image if
        it was run in a browser.
        """
        self.__screenshots = list()
        if (self.message):
            if (self.browser or self.platform or self.version):
                prefix = '{0}/'.format('-'.join([self.browser, self.platform, self.version]))
            else:
                prefix = None
            for screenshot in SCREENSHOT_REGEX.findall(self.message):
                self.__screenshots.append(urlparse.urljoin(prefix, screenshot))

    #   States

    @property
    def skipped(self):
        """Determines if the test was skipped."""
        return self.__test_case.skipped

    @property
    def succeeded(self):
        """Determines if the test was a success."""
        return self.__test_case.good

    @property
    def failed(self):
        """Determines if the test failed."""
        return self.__test_case.failed

    @property
    def errored(self):
        """Determines if there was an error during the test."""
        return self.__test_case.errored

    # /States

    @property
    def number(self):
        """Test number in this run."""
        return self.__number

    @property
    def browser(self):
        """Browser used in this run."""
        return self.__browser

    @property
    def platform(self):
        """Platform used in this run."""
        return self.__platform

    @property
    def version(self):
        """Version used in this run."""
        return self.__version

    @property
    def result(self):
        """Test result message."""
        return self.__test_case.result

    @property
    def time(self):
        """Time (in ms) to run the test."""
        return int(self.__test_case.time.total_seconds() * 1000.)

    @property
    def traceback(self):
        """Get the traceback."""
        return self.__test_case.trace

    @property
    def message(self):
        """Displays all the text generated during the test run."""
        return self.__test_case.message

    @property
    def screenshots(self):
        """List of screenshots associated with this test case."""
        return self.__screenshots
