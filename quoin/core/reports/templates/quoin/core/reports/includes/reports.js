(function($) {

    $(function() {
        // initialize #results-table as a dataTable
        var oTable = $('#results-table').dataTable({
            "bPaginate": false,
            "bSort": true,
            "aaSorting": [[ 0, "asc" ]],
            "sDom": ''
        });

        // update the summary stats based on the currently filtered table
        function updateSummary(){
            // get values based
            var total = $('tbody tr.multi-test-case').length;
            var success = $('tbody tr.multi-test-case.success').length;
            var error = $('tbody tr.multi-test-case.error').length;
            var failure = $('tbody tr.multi-test-case.failure').length;
            var skipped = $('tbody tr.multi-test-case.skipped').length;

            // compute fractions & percentage strings
            var success_fraction = parseFloat((100 * (success/total)).toFixed(2));
            var error_fraction  = parseFloat((100 * (error/total)).toFixed(2));
            var failure_fraction  = parseFloat((100 * (failure/total)).toFixed(2));
            var skipped_fraction  = parseFloat((100 - success_fraction - error_fraction - failure_fraction).toFixed(2));

            var success_percentage = success_fraction ? (success_fraction + '%') : '0%';
            var error_percentage = error_fraction ? error_fraction + '%' : '0%';
            var failure_percentage = failure_fraction ? failure_fraction + '%' : '0%';
            var skipped_percentage = skipped_fraction ? skipped_fraction + '%' : '0%';

            // update label totals
            $('div.total .label').text(total);
            $('div.success .label').text(success).attr('data-original-title', success_percentage);
            $('div.error .label').text(error).attr('data-original-title', error_percentage);
            $('div.failure .label').text(failure).attr('data-original-title', failure_percentage);
            $('div.skipped .label').text(skipped).attr('data-original-title', skipped_percentage);

            // update progress bar
            $('div.progress .progress-bar-success').width(success_percentage).attr('data-original-title', success_percentage);
            $('div.progress .progress-bar-danger').width(error_percentage).attr('data-original-title', error_percentage);
            $('div.progress .progress-bar-warning').width(failure_percentage).attr('data-original-title', failure_percentage);
            $('div.progress .progress-bar-info').width(skipped_percentage).attr('data-original-title', skipped_percentage);
        }

        // filter the table on a column. The filter name and column are required data-filter-x attributes on
        // the button's parent, the filter value is a data-filter-value attribute on the button itself.
        function filterTable(button){
            // toggle css
            $(button).toggleClass('btn-disabled');
            $(button).find('.glyphicon').toggleClass('glyphicon-remove').toggleClass('glyphicon-ok');

            var selectedFilters = [];
            $(button).parent().children('button:not(".btn-disabled")').each(function() {
                selectedFilters.push($(this).attr('data-filter-value'));
            });

            // if no filters then filter string is null, else its ORed like: "(Foo|Bar)"
            var filterString = (selectedFilters.length === 0) ? null : ('(' + selectedFilters.join('|') + ')');
            oTable.fnFilter(filterString, $(button).parent().attr('data-filter-column'), true);

            // update summary stats
            updateSummary();
        }

        // text filter
        $('#filter-text').on("keyup", function(e){
            e.preventDefault();
            oTable.fnFilter(this.value);
            updateSummary();
        });

        // don't submit form on enter since we're filtering the inline table
        $('#text-filter form').on("submit", function(e){
            e.preventDefault();
        });

        // initialize tooltips
        $('[data-toggle="tooltip"]').tooltip();

        // event listeners for filtering buttons
        $('.filters button').on('click', function(e){
            filterTable(this);
        });

        // show the modal with a given title and body
        var display_modal = function(title, content) {
            $('#modal .modal-title').text(title);
            $('#modal div.modal-body').empty().append(content);
            $('#modal').modal();
        };

        // open modal with screenshot on click
        $('a.screenshot-open').on('click', function(event) {
            event.preventDefault();
            var img_src = $(this).attr('data-img');
            display_modal(img_src, $('<div>').addClass('scroll').append($('<img>').attr('src', img_src)));
        });

        // open modal with error message on click
        $('a.result-open').on('click', function(event) {
            event.preventDefault();
            display_modal('Log Output', $('<div>').addClass('scroll message').text($(this).attr('data-message')));
        });

        // update the summary on page load
        updateSummary();

    });

}(jQuery));
