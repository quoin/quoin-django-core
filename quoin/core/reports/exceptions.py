#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Exceptions for Quoin's core test report app.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: DATE
Organization: Quoin Inc.
Copyrights:
"""


__docformat__ = 'restructuredtext en'

__all__ = (
        'TestReportException',
        'InvalidTestDirectory',
        'NoTestDirectoriesAvailable',
        )


class TestReportException(Exception):
    """
    Generic exception for Quoin's core test reporting app.
    """


class InvalidTestDirectory(TestReportException):
    """
    Raised when the value of env. variable `CURRENT_TEST_DIR` is invalid.
    """


class NoTestDirectoriesAvailable(TestReportException):
    """
    Raises when searching for the latest test directory, but none exists.
    """
