#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Settings for the app.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.12
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        )



import os

from quoin.core.reports import constants
from quoin.core.reports import directory
from quoin.core.reports import exceptions



def update(module):
    """
    Updates the settings with app's settings.
    """

    if ('quoin.core.reports' not in module.INSTALLED_APPS):
        module.INSTALLED_APPS += ('quoin.core.reports', )

    try:
        module.TEST_REPORT_ROOTDIR = os.path.join(module.QUOIN_ROOT, 'reports', 'quoin')
    except AttributeError:
        raise exceptions.TestReportException("Missing QUOIN_ROOT setting.")

    try:
        module.CURRENT_TEST_DIR_ENV_NAME # Is it defined?
    except AttributeError:
        module.CURRENT_TEST_DIR_ENV_NAME = constants.CURRENT_TEST_DIR_ENV_NAME

    dest_dir = directory.Directory.get_directory(module)

    module.TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
    module.XUNIT_FILE = os.path.join(dest_dir, 'nosetests.xml')
    module.NOSE_ARGS = [
            #'-vvv',
            '--exe',
            '--with-coverage',
            '--cover-package={0}'.format(module.__name__.split('.')[0]),
            '--cover-html',
            '--cover-html-dir={0}'.format(os.path.join(dest_dir, 'coverage')),
            #'--cover-inclusive',
            '--cover-branches',
            '--nocapture',
            '--with-xunit',
            '--xunit-file={0}'.format(module.XUNIT_FILE),
            ]


    #   Formatters

    module.LOGGING['formatters']['test'] = {
            'format': '%(asctime)s [%(process)d] [%(levelname)s] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'class': 'logging.Formatter',
            }

    module.LOGGING['formatters']['testrun'] = {
            'format': '%(asctime)s [%(process)d] [%(levelname)s] [%(testid)s] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'class': 'logging.Formatter',
            }

    #   Handlers

    module.LOGGING['handlers']['test'] = {
            'class': 'logging.FileHandler',
            'formatter': 'test',
            'filename': os.path.join(dest_dir, 'test.log'),
            }

    module.LOGGING['handlers']['testrun'] = {
            'class': 'logging.FileHandler',
            'formatter': 'testrun',
            'filename': os.path.join(dest_dir, 'testrun.log'),
            }

    #   Loggers

    module.LOGGING['loggers']['test'] = {
            'level': 'DEBUG',
            'handlers': ['test'],
            'propagate': False,
            }

    module.LOGGING['loggers']['testrun'] = {
            'level': 'DEBUG',
            'handlers': ['testrun'],
            'propagate': True,
            }


