#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Constant module.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.16
Organization: Quoin Inc.
Copyrights:
"""


__docformat__ = 'restructuredtext en'

__all__ = (
        )

import re


CURRENT_TEST_DIR_ENV_NAME = 'CURRENT_TEST_DIR'

# Regex for directory name: 0001_201309121528 seq:1 on 09/12/2013 03:28pm
DIR_NAME_REGEX = re.compile(r'^(\d{4})_(\d{12})$')

DISPLAY_DATE_FORMAT = '%m/%d/%Y %I:%M%p'
FILE_DATE_FORMAT = '%Y%m%d%H%M'

NO_DOCSTRING = '*** NO DOCSTRING DEFINED ***'

SCREENSHOT_REGEX = re.compile(r'<screenshot>(.*)</screenshot>')