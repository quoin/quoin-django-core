#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Management command for compiling test results.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.10
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'Command',
        )




from django.core.management import base

from quoin.core.reports import report




class Command(base.NoArgsCommand):
    """
    Management command definition for `compile_test_results`.
    """
    help = """Compile test results into html pages."""


    def handle(self, *args, **options):
        """
        Handling management command.
        """
        report.Report.generate()

