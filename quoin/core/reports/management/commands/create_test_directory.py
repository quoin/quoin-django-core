#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Management command to create a testing report environment.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.16
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'Command',
        )



from django.conf import settings
from django.core.management import base

from quoin.core.reports import directory



class Command(base.NoArgsCommand):
    """
    Creates a directory for the outputs of tests.
    """

    help = """Creates a directory to run tests."""


    def handle(self, *args, **options):
        """
        Handling management command.
        """
        dest_dir = directory.Directory.create()
        return "{0}={1}".format(settings.CURRENT_TEST_DIR_ENV_NAME, dest_dir)
