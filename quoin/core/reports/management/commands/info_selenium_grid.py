#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Retrieve selenium grid information.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.18
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        )



import json

from django.core.management import base

from quoin.core.tests.selenium import grid



class Command(base.NoArgsCommand):
    """
    Management command to get selenium grid information.
    """
    help = """Extract selenium grid information."""


    def handle(self, *args, **options):
        """
        Handling management command.
        """
        hub = grid.parse_grid()
        return json.dumps(hub)
