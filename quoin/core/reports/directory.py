#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Module to create the directory to output tests results.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.13
Organization: Quoin Inc.
Copyrights:
"""


__docformat__ = 'restructuredtext en'

__all__ = (
        'Directory',
        )


import datetime
import os

from quoin.core.reports.constants import DIR_NAME_REGEX, FILE_DATE_FORMAT
from quoin.core.reports import exceptions


class Directory(object):
    """
    Testing report directory.
    """

    @classmethod
    def create(cls):
        """
        Next directory.
        """
        from django.conf import settings  # Circular import

        max_num = 0
        for directory in os.listdir(settings.TEST_REPORT_ROOTDIR):
            dir_match = DIR_NAME_REGEX.match(directory)
            if (dir_match is not None):
                max_num = max(max_num, int(dir_match.group(1)))

        dest_dir = os.path.join(settings.TEST_REPORT_ROOTDIR, '{0:04d}_{1:12s}'.format(max_num + 1, datetime.datetime.now().strftime(FILE_DATE_FORMAT)))
        os.mkdir(dest_dir)  # Create it right away to reserve it.
        return dest_dir

    @classmethod
    def get_directory(cls, module):
        """
        Finds the directory to use. If the env. var `CURRENT_TEST_DIR` is
        defined, use it.
        """
        dest_dir = os.environ.get(module.CURRENT_TEST_DIR_ENV_NAME)
        if (dest_dir is not None):
            if (not os.path.isdir(dest_dir)):
                raise exceptions.InvalidTestDirectory("Invalid value for {0}: {1}".format(module.CURRENT_TEST_DIR_ENV_NAME, dest_dir))
        else:
            max_num = 0
            max_dir = None
            for directory in os.listdir(module.TEST_REPORT_ROOTDIR):
                dir_match = DIR_NAME_REGEX.match(directory)
                if (dir_match is not None):
                    this_num = int(dir_match.group(1))
                    if (this_num > max_num):
                        max_num = this_num
                        max_dir = directory
            if (max_dir is None):
                return ''
            dest_dir = os.path.join(module.TEST_REPORT_ROOTDIR, max_dir)
            os.environ[module.CURRENT_TEST_DIR_ENV_NAME] = dest_dir  # To avoid recomputing everytime.
        return dest_dir
