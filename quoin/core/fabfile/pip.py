#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Pip related fabric tasks.

Author: Matthew Foy <matthew.foy@quoininc.com>
        Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.07
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'freeze',
        'install',
        'list_upgradeable',
        'upgrade_all',
        )



from fabric import api

from quoin.core import fabfile



@api.task(task_class=fabfile.VirtualEnvTask)
def freeze():
    """
    Display list of installed python libraries.
    """
    fabfile.local("pip freeze --local")

@api.task(task_class=fabfile.VirtualEnvTask)
def list_upgradeable():
    """
    List python libraries that are upgradeable.
    """
    fabfile.local("pip freeze --local | grep -v '-e ' | cut -d = -f 1 | xargs -n 1 pip search | grep -B2 'LATEST:'")

@api.task(task_class=fabfile.VirtualEnvTask)
def install():
    """
    Install python libraries for the desired environment.
    """
    fabfile.local("pip install -r requirements/%(name)s.txt" % env)

@api.task(task_class=fabfile.VirtualEnvTask)
def upgrade_all():
    """
    Upgrade all python libraries.
    """
    fabfile.local("pip freeze --local | grep -v '-e ' | cut -d = -f 1  | xargs pip install --upgrade")
