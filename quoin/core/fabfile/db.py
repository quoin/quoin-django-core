#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Database related fabric tasks.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.20
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'migrate',
        )



from fabric import api
from quoin.core import fabfile



@api.task(task_class=fabfile.VirtualEnvTask)
def migrate():
    """
    Sync and migrate the database.
    """
    fabfile.manage_py('syncdb --noinput')
    fabfile.manage_py('syncdb --migrate')
