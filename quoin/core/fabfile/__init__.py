#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
fabric package.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.16
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        )



import contextlib

from fabric import api
from fabric import tasks



# setup environment
api.env.hosts = ['localhost', ]

# User account name, should not be needed for local dev tasks, e.g.
# env.user = 'dev'
api.env.user = ''

# Used to determine pip requirements and django settings, one of 'dev', 'qa', 'production', e.g.
# env.name = 'dev'
api.env.name = ''

# Path where reports are written, e.g.
# env.reports = '/path/to/reports'
api.env.reports = '/tmp'

# The directory to change to on activating the virtualenv, e.g.
# env.directory = '/home/dev/code/marketing/django'
api.env.directory = '/tmp/'

# Command to activate your virtual env, e.g.
# env.activate = 'source /path/to/virtualenvs/project/bin/activate'
api.env.activate = ''

# Don't break on error, maybe this is bad...
api.env.warn_only = True


# import active.py to override env
# TODO - how do we really set env for fabric?
try:
    from fabfile import active
except ImportError:
    raise SystemExit("Create 'fabfile/active.py' to set the Fabric env variables.")




def local(cmd, capture=False):
    """
    Execute a local command.
    """
    res = api.local(cmd, shell='/bin/bash', capture=capture)
    if ((capture is True) and (res.return_code)):
        # Something went wrong.
        print("{0} STDOUT {0}".format('-' * 20))
        print(res.stdout)
        print(res.stdout)
        print("{0} STDERR {0}".format('-' * 20))
        print(res.stderr)
        print(res.stderr)
        print("{0} END {0}".format('-' * 20))
        return None
    else:
        return res


def manage_py(cmd, capture=False):
    """
    Execute './manage.py' with the command `cmd`.
    """
    return local('./manage.py {0}'.format(cmd), capture=capture)


@contextlib.contextmanager
def virtualenv():
    """
    Change directory and enable the virtualenv.
    """
    with api.lcd(api.env.directory), api.prefix(api.env.activate): # pylint: disable=C0321
        yield


class QuoinTask(tasks.Task):
    """
    Quoin base implementation of a task to keep the task's docstring.
    """

    #: Function decored
    func = None

    VIRTUALENV = False
    DJANGO_SETTINGS_MODULE = None

    def __init__(self, func, *args, **kwargs):
        super(QuoinTask, self).__init__(*args, **kwargs)
        self.func = func
        self.__doc__ = func.__doc__


    def run(self, *args, **kwargs):
        if (self.VIRTUALENV):
            if (self.DJANGO_SETTINGS_MODULE):
                with virtualenv(), api.shell_env(DJANGO_SETTINGS_MODULE=self.DJANGO_SETTINGS_MODULE): # pylint: disable=C0321
                    return self.func(*args, **kwargs)
            else:
                with virtualenv():
                    return self.func(*args, **kwargs)
        elif (self.DJANGO_SETTINGS_MODULE):
            with api.shell_env(DJANGO_SETTINGS_MODULE=self.DJANGO_SETTINGS_MODULE):
                return self.func(*args, **kwargs)
        else:
            return self.func(*args, **kwargs)


class VirtualEnvTask(QuoinTask):
    """
    Run the code in a virtual environment.
    """

    VIRTUALENV = True



class TestTask(VirtualEnvTask):
    """
    The project should overwrite this value.
    """
    DJANGO_SETTINGS_MODULE = None



@api.task(task_class=VirtualEnvTask)
def clean():
    """
    Clean up the environment.
    """
    manage_py('cleanup')
    manage_py('clean_pyc')
    local('''find . -name '*.py[co]' -print0 | xargs -0 rm''')


#from quoin.core.fabfile import test
def update(module, django_settings_module):
    """
    Updates the fabfile module to include common tasks.
    """
    from quoin.core.fabfile import db
    from quoin.core.fabfile import django
    from quoin.core.fabfile import gunicorn
    from quoin.core.fabfile import nginx
    from quoin.core.fabfile import pip
    from quoin.core.fabfile import test

    TestTask.DJANGO_SETTINGS_MODULE = django_settings_module

    module.clean = clean
    module.db = db
    module.django = django
    module.gunicorn = gunicorn
    module.nginx = nginx
    module.pip = pip
    module.test = test
