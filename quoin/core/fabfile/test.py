#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
test related fabric files.

Author: Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.09.16
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'browsers',
        'clean',
        'cross',
        'report',
        'selenium',
        'test',
        'unit',
        'views',
        )



import os
import json

from fabric import api

from quoin.core import fabfile



CURRENT_TEST_DIR_ENV_NAME = None


def _manage_py(cmd, capture=False):
    """
    Execute `manage.py` with cmd. If `capture` is True, make sure the command
    did not produce any errors.
    """
    res = api.local('./manage.py {0}'.format(cmd), shell='/bin/bash', capture=capture)
    if (capture and res.return_code):
        # Something went wrong.
        print("{0} STDOUT {0}".format('-' * 20))
        print(res.stdout)
        print("{0} STDERR {0}".format('-' * 20))
        print(res.stderr)
        print("{0} END {0}".format('-' * 20))
        return None
    return res


def _create_current_test_dir():
    """
    Creates a directory for the current test run.
    """
    capture = _manage_py('create_test_directory', capture=True)
    if (capture):
        global CURRENT_TEST_DIR_ENV_NAME # pylint: disable=W0603
        (CURRENT_TEST_DIR_ENV_NAME, dest_dir) = capture.strip().split('=', 1)
        os.environ[CURRENT_TEST_DIR_ENV_NAME] = dest_dir
        print("Generating tests in {0!r}.".format(dest_dir))
        return dest_dir
    else:
        return None


def _generate_current_test_report():
    """
    Generates the test report.
    """
    capture = _manage_py('compile_test_results', capture=True)
    if (capture):
        return 'OK'
    else:
        return None


def _local(*args, **kwargs):
    """
    Call the `api.local()`.
    """

    if (kwargs.get('exclude_selenium')):
        args += ('--exclude=selenium', )
    else:
        args += ('--liveserver=0.0.0.0:8081', )
        os.environ['SELENIUM_BROWSER'] = kwargs.get('browser', 'firefox')
        os.environ['SELENIUM_PLATFORM'] = kwargs.get('os', 'ANY')
        os.environ['SELENIUM_BROWSER_VERSION'] = kwargs.get('version', '')

    if (kwargs.get('exclude_views')):
        args += ('--exclude=views',)

    if (kwargs.get('exclude_unit')):
        args += ('--exclude=unit',)

    _manage_py('test --noinput {0}'.format(' '.join(args)))


def _run(*args, **kwargs):
    """
    Call to run the tests.
    """
    if (_create_current_test_dir()):
        _local(*args, **kwargs)
        _generate_current_test_report()


@api.task(task_class=fabfile.TestTask, default=True)
def test(*args, **kwargs):
    """
    Run all tests, with any optional args provided (to run specific test case for example).
    """
    _run(with_liveserver=True, *args, **kwargs)


@api.task(task_class=fabfile.TestTask)
def unit(*args, **kwargs):
    """
    Run unit tests only.
    """
    _run(exclude_selenium=True, exclude_views=True, *args, **kwargs)


@api.task(task_class=fabfile.TestTask)
def views(*args, **kwargs):
    """
    Run views tests only.
    """
    _run(exclude_unit=True, exclude_selenium=True, *args, **kwargs)


@api.task(task_class=fabfile.TestTask)
def selenium(*args, **kwargs):
    """
    Run selenium tests only.
    """
    _run(exclude_unit=True, exclude_views=True, *args, **kwargs)


def _cross_browser(browser_only, *args, **kwargs):
    """
    Test cross browser. If `browser_only` is `True`, only test on a single
    instance of that browser, on any OS and in any version.
    """
    capture = _manage_py('info_selenium_grid', capture=True)
    if (capture is not None):
        data = json.loads(capture.strip())

        if (_create_current_test_dir()):
            _local(exclude_selenium=True, *args, **kwargs)
            global_current_test_dir = os.environ[CURRENT_TEST_DIR_ENV_NAME]
            for (browser_name, browser_os, browser_version) in data:
                if (browser_only is True):
                    browser_os = 'ANY'
                    browser_version = ''
                _browser_path = os.path.join(global_current_test_dir, '-'.join([browser_name, browser_os, browser_version]))
                os.environ[CURRENT_TEST_DIR_ENV_NAME] = _browser_path
                if (os.path.isdir(_browser_path)):
                    # Has already been run. This happens when `browser_only is True`.
                    continue

                os.mkdir(_browser_path)
                _local(exclude_unit=True, exclude_views=True, browser=browser_name, os=browser_os, version=browser_version, *args, **kwargs)
            os.environ[CURRENT_TEST_DIR_ENV_NAME] = global_current_test_dir
            _generate_current_test_report()


@api.task(task_class=fabfile.TestTask)
def cross(*args, **kwargs):
    """
    Test for multiple browsers/versions/oses.
    """
    _cross_browser(browser_only=False, *args, **kwargs)


@api.task(task_class=fabfile.TestTask)
def browsers(*args, **kwargs):
    """
    Run all tests, but only once for each browser, letting the grid select os and version.
    """
    _cross_browser(browser_only=True, *args, **kwargs)


@api.task(task_class=fabfile.TestTask)
def clean():
    """
    Remove prior test run coverage results.
    """
    api.local('''find . -name '.coverage' -print0 | xargs -0 rm''', shell='/bin/bash')



@api.task(task_class=fabfile.TestTask)
def report():
    """
    Regenerate the report for the last test run.
    """
    _manage_py('compile_test_results')
