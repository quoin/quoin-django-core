#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
nginx related fabric tasks.

Author: Matthew Foy <matthew.foy@quoininc.com>
        Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.07
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'restart',
        'start',
        'stop',
        )



from fabric import api

from quoin.core import fabfile



@api.task
def stop():
    """
    Stop the nginx server.
    """
    fabfile.local("sudo /etc/init.d/nginx stop")

@api.task
def start():
    """
    Start the nginx server.
    """
    fabfile.local("sudo /etc/init.d/nginx start")

@api.task(default=True)
def restart():
    """
    Restart the nginx server.
    """
    stop()
    start()
