#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Gunicorn related fabric tasks.

Author: Matthew Foy <matthew.foy@quoininc.com>
        Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.13
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'restart',
        'start',
        'stop',
        )



from fabric import api

from quoin.core import fabfile



@api.task(task_class=fabfile.VirtualEnvTask)
def stop():
    """
    Stop the gunicorn server.
    """
    fabfile.local("pkill gunicorn")


@api.task(task_class=fabfile.VirtualEnvTask)
def start():
    """
    Start the gunicorn server.
    """
    fabfile.local("gunicorn_django -D -c %(directory)s/../gunicorn/gunicorn.conf.py" % api.env)


@api.task(default=True)
def restart():
    """
    Restart the gunicorn server.
    """
    stop()
    start()
