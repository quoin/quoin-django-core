#!/usr/bin/env python
# -*- mode: Python; coding: UTF-8 -*-

"""\
Django related fabric tasks.

Author: Matthew Foy <matthew.foy@quoininc.com>
        Huu Da Tran <huuda.tran@quoininc.com>
Since: 2013.08.13
Organization: Quoin Inc.
Copyrights:
"""



__docformat__ = 'restructuredtext en'

__all__ = (
        'collect_static',
        'rebuild_search_index',
        'runserver',
        'update_search_index',
        )



from fabric import api

from quoin.core import fabfile



@api.task(task_class=fabfile.VirtualEnvTask, default=True)
def runserver():
    """ Run the test server. """
    fabfile.manage_py("runserver 0.0.0.0:8081")


@api.task(task_class=fabfile.VirtualEnvTask)
def rebuild_search_index():
    """ Drop and recreate the search index from scratch. """
    fabfile.manage_py("rebuild_index --noinput")


@api.task(task_class=fabfile.VirtualEnvTask)
def update_search_index():
    """ Just update the search index. """
    fabfile.manage_py("update_index")


@api.task(task_class=fabfile.VirtualEnvTask)
def collect_static(clear=False):
    """ Collect static content. """
    fabfile.manage_py("build_urls_js")
    fabfile.manage_py("collectstatic --noinput %s" % ("--clear" if clear else ""))
