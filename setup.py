#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

"""
Setup file for the Quoin Django core package.
"""



import setuptools



setuptools.setup(name='quoin-django-core',
        version='0.1.7', # Remember to update requirements/base.txt in your project.
        author="""Quoin Inc.""",
        author_email='info@quoininc.com',
        packages=setuptools.find_packages(),
        url='http://www.quoininc.com/',
        description="""Quoin Django core.""",
        long_description=open('README.txt').read(),
        zip_safe=False, # No egg.
        include_package_data=True,
        install_requires=[
            'Django==1.5.2',
            'Fabric==1.7.0',
            'Jinja2==2.7.1',
            'MarkupSafe==0.18',
            'Pillow==2.1.0',
            'PyYAML==3.10',
            'Pygments==1.6',
            'South==0.8.2',
            'Sphinx==1.2b1',
            'astroid==1.0.0',
            'autopep8==0.9.2',
            'beautifulsoup4==4.3.1',
            'coverage==3.6',
            'django-admin-tools==0.4.0',
            'django-analytical==0.15.0',
            'django-crispy-forms==1.3.2',
            'django-debug-toolbar==0.9.4',
            'django-extensions==1.1.1',
            'django-haystack==2.1.0',
            'django-memcache-status==1.1',
            'django-mptt==0.6.0',
            'django-nose==1.2',
            'django-pipeline==1.3.14',
            'django-robots==0.9.2',
            'django-secure==1.0',
            'django-tinymce==1.5.1',
            'docutils==0.11',
            'feedparser==5.1.3',
            'futures==2.1.4',
            'ipython==1.0.0',
            'logilab-astng==0.24.3',
            'logilab-common==0.60.0',
            'lxml==3.2.3',
            'mimeparse==0.1.3',
            'nose==1.3.0',
            'pep8==1.4.6',
            'psycopg2==2.5.1',
            'pycrypto==2.6',
            'pyflakes==0.7.3',
            'pylibmc==1.2.3',
            'pylint==1.0.0',
            'pyparsing==2.0.1',
            'python-dateutil==2.1',
            'pytz==2013d',
            'requests==1.2.3',
            'selenium==2.35.0',
            'simplejson==3.3.0',
            'six==1.3.0',
            'xunitparser==1.2.0',
            ],
        )

